## Grammar
* [parts of speech](https://www.englishclub.com/grammar/parts-of-speech.htm)
* [target structures](https://www.udemy.com/course/learn-english-grammar-upgrade-your-speaking-and-listening/)
* [irregular verbs](https://www.englisch-hilfen.de/en/grammar/unreg_verben1.htm)
* [tenses](https://www.englisch-hilfen.de/en/grammar/english_tenses.htm)
* [punctuation](https://www.wikihow.com/Use-English-Punctuation-Correctly)

## Anki
* [irregular verbs](https://ankiweb.net/shared/info/110667962) 
* [400 words for toefl](https://ankiweb.net/shared/info/1708037740)
* [english grammar in use](https://ankiweb.net/shared/info/715945745)
