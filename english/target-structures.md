## had better

means that you should do something. If you don't do this thing, there will be a problem or danger in the future.

## enough and too

This bed is big enough - It's a good size for me. \
This bed is not big enough - It needs to be bigger \
This bed is too big - It needs to be smaller

adjective/adverb + enough \
enough + noun \
too + adjective 

> The zebra ran quickly enough to escape from the lion.

## used to 

something happened often in the past, but doesn't happen now

I/we/he/she/etc. + used to + basic verb (be, go, play etc.)
> She used to love chocolate, but now she doesn't eat it.

I/we/he/she/etc. + didn't + used to + basic verb
> We didn't use to enjoy baseball, but now we love it.

I/we/he/she/etc. + never + use to + basic verb
> I never use to eat spicy food, but now Ireally enjoy it.

Did +you/he/she/etc. + use to + basic verb
> Did you use to live in New York?

## present continuous

the action is continuing and isn't finished.\
she has started it, but she hasn't finished it.\
it is changing over this time period.

Am/is/are + ing verb
> I am eating dinner = I'm eating dinner

Am/is/are + not + ing verb
> it is not doing = it's not or it isn't doing 
>
> They are not doing = they're not or they aren't doing

Am/is/are + subject + ing verb
> Are you looking in the right place?

## verb + -ing 

verbs which use an -ing verb form after them.

I/he/she/it etc. + **enjoy** + -ing verb form (walking, visiting etc.)
* **enjoy**
* **love**
* **hate**
* **stop**
* **avoid**
* **imagine**
* **consider**
* **spend time**
* **practise**
* **finish**

> whenever I'm angry,I ***imagine*** walking along a beautiful beach. my anger quickly disappears. 

I/he/she/it etc. + verb from list + not + -ing verb form

> the summer holidays are here! I enjoy not waking up early every day.

question word + I/he/she/it etc. + verb from list + -ing verb form

> Do you enjoy eating out?

> I ***enjoy watching*** romance movies and action movies.
>
> I ***enjoy*** romance movies and action movies.

the meaning of these two sentences is very similar, but the first is emphasizing that you enjoy **watching** these movies

the tense of the verbs can change, but the -ing form must still be usedafter.

> I loved eating chocolate when I was a child.
> 
> I love eating new cuisine from around the world.
> 
> she loves eating at expensive restaurants.

the verb + -ing structure is different to the present continuous (be + -ing):\
verb + -ing: I enjoy travelling around France (generally, I like travelling around France).\
present continuous: I am travelling around France (I am travelling around France now).

be + -ing + verb + -ing

> My friend Barry is considering moving to Australia. He has always wanted to live there.
