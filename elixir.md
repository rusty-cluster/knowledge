## Books

* [Programming Ecto](https://pragprog.com/titles/wmecto/programming-ecto/)
* [Craft GraphQL APIs in Elixir with Absinthe](https://pragprog.com/titles/wwgraphql/craft-graphql-apis-in-elixir-with-absinthe/)

## Courses

* [The complete Elixir and Phoenix Bootcamp](https://www.udemy.com/course/the-complete-elixir-and-phoenix-bootcamp-and-tutorial/)
* [Elixir for Programmers](https://codestool.coding-gnome.com/courses/elixir-for-programmers-2)

## Anti-patterns

* [lucasvegi/Elixir-Code-Smells](https://github.com/lucasvegi/Elixir-Code-Smells)
