## Владимир Спиридонов

1. Что такое эвристические стратегии? https://youtu.be/VOmvu9kPIPc
2. Мозговой штурм https://youtu.be/1f61n27N6Qo
3. Синектика https://youtu.be/wnHXQ3158vA
4. Аналитические эвристики https://youtu.be/4nI1EmdgJ1o
5. Решение задач: морфологические карты Цвикки https://youtu.be/TYoytc77KBo
6. Гирлянды ассоциаций: метод фокальных объектов https://youtu.be/sV9mtTQdpec
7. Генерация идей и рефлексия https://youtu.be/rrhPx2138Hk
